package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.empty.EmptyNameException;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.dto.SessionDTO;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public class TaskServiceTest {

    @Nullable
    private TaskService taskService;

    @Nullable
    private TaskDTO task;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private SessionDTO session;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID = "TestUserId";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_TASK_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
        taskService = new TaskService(connectionService);
        taskService.create(session.getUserId(), TEST_TASK_NAME, TEST_DESCRIPTION_NAME);
        this.task = taskService.findByName(session.getUserId(), TEST_TASK_NAME);
    }

    @After
    public void after() {
        taskService.clear(session.getUserId());
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
        Assert.assertEquals(TEST_DESCRIPTION_NAME, task.getDescription());

        @NotNull final TaskDTO taskById = taskService.findById(session.getUserId(), task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    public void findAllByUserId() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(session.getUserId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @Nullable final TaskDTO task = taskService.findById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final TaskDTO task = taskService.findById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @Nullable final TaskDTO task = taskService.findById(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final TaskDTO task = taskService.findById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskService.removeById(session.getUserId(), task.getId());
        Assert.assertNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void findByName() {
        @Nullable final TaskDTO task = taskService.findByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @Nullable final TaskDTO task = taskService.findByName(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @Nullable final TaskDTO task = taskService.findByName(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @Nullable final TaskDTO task = taskService.findByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @NotNull final TaskDTO task = taskService.findByIndex(session.getUserId(), 0);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskService.removeById(session.getUserId(), task.getId());
        Assert.assertNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        taskService.removeById(session.getUserId(), null);
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        taskService.removeById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void removeByIdIncorrectUser() {
        taskService.removeById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void removeByName() {
        taskService.removeByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void removeByNameIncorrect() {
        taskService.removeByName(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName(session.getUserId(), null);
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void removeByNameIncorrectUser() {
        taskService.removeByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNotNull(taskService.findById(session.getUserId(), task.getId()));
    }

    @Test
    public void startById() {
        @Nullable final TaskDTO task = taskService.startById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void startByIdNull() {
        @Nullable final TaskDTO task = taskService.startById(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIdIncorrect() {
        @Nullable final TaskDTO task = taskService.startById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIdIncorrectUser() {
        @Nullable final TaskDTO task = taskService.startById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void startByName() {
        @Nullable final TaskDTO task = taskService.startByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void startByNameNull() {
        @Nullable final TaskDTO task = taskService.startByName(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrect() {
        @Nullable final TaskDTO task = taskService.startByName(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByNameIncorrectUser() {
        @Nullable final TaskDTO task = taskService.startByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void startByIndex() {
        @Nullable final TaskDTO task = taskService.startByIndex(session.getUserId(), 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIndexIncorrect() {
        @Nullable final TaskDTO task = taskService.startByIndex(session.getUserId(), 674);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void startByIndexIncorrectUser() {
        @Nullable final TaskDTO task = taskService.startByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void finishById() {
        @Nullable final TaskDTO task = taskService.finishById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = EmptyIdException.class)
    public void finishByIdNull() {
        @Nullable final TaskDTO task = taskService.finishById(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIdIncorrect() {
        @Nullable final TaskDTO task = taskService.finishById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIdIncorrectUser() {
        @Nullable final TaskDTO task = taskService.finishById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void finishByName() {
        @Nullable final TaskDTO task = taskService.finishByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = EmptyNameException.class)
    public void finishByNameNull() {
        @Nullable final TaskDTO task = taskService.finishByName(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrect() {
        @Nullable final TaskDTO task = taskService.finishByName(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByNameIncorrectUser() {
        @Nullable final TaskDTO task = taskService.finishByName(TEST_USER_ID_INCORRECT, TEST_TASK_NAME);
        Assert.assertNull(task);
    }

    @Test
    public void finishByIndex() {
        @Nullable final TaskDTO task = taskService.finishByIndex(session.getUserId(), 0);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIndexIncorrect() {
        @Nullable final TaskDTO task = taskService.finishByIndex(session.getUserId(), 674);
        Assert.assertNull(task);
    }

    @Test(expected = TaskNotFoundException.class)
    public void finishByIndexIncorrectUser() {
        @Nullable final TaskDTO task = taskService.finishByIndex(TEST_USER_ID_INCORRECT, 674);
        Assert.assertNull(task);
    }

    @Test
    public void changeStatusById() {
        @Nullable final TaskDTO task = taskService.changeStatusById(session.getUserId(), this.task.getId(), Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByName() {
        @Nullable final TaskDTO task = taskService.changeStatusByName(session.getUserId(), TEST_TASK_NAME, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        @Nullable final TaskDTO task = taskService.changeStatusByIndex(session.getUserId(), 0, Status.NOT_STARTED);
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getStatus());
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
    }

}