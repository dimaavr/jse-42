package ru.tsc.avramenko.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskRepository {

    @Select("SELECT * FROM `app_task` WHERE `project_id`=#{projectId} AND `user_id`=#{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAllTaskByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull String projectId);

    @Select("SELECT * FROM `app_task` WHERE `id`=#{id} AND `user_id`=#{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findById(@Param("userId") final String userId, @Param("id") final String id);

    @Select("SELECT * FROM `app_task` WHERE `name`=#{name} AND `user_id`=#{userId} LIMIT 1")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findByName(@Param("userId") final String userId, @Param("name") final String name);

    @Select("SELECT * FROM `app_task` WHERE `user_id`=#{userId} LIMIT 1 OFFSET #{index}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    TaskDTO findByIndex(@Param("userId") final String userId, @Param("index") final int index);

    @Delete("DELETE FROM `app_task` WHERE `id` = #{id} AND `user_id` = #{userId}")
    void removeById(@Param("userId") final String userId, @Param("id") final String id);

    @Delete("DELETE FROM `app_task` WHERE `name` = #{name} AND `user_id` = #{userId}")
    void removeByName(@Param("userId") final String userId, @Param("name") final String name);

    @Delete("DELETE FROM `app_task` WHERE `user_id` = #{userId} OFFSET #{index}")
    void removeByIndex(@Param("userId") final String userId, @Param("index") final int index);

    @Update("UPDATE `app_task` " +
            "SET `name`=#{name}, `description`=#{description}, `status`=#{status}, `start_date`=#{startDate}, " +
            "`finish_date`=#{finishDate}, `created_date`=#{created}, " +
            "`user_id`=#{userId}, `project_id`=#{projectId} WHERE `id` = #{id}")
    void update(@NotNull TaskDTO task);

    @Update("UPDATE `app_task` " +
            "SET `project_id` = null WHERE `project_id` = #{projectId} AND `user_id` = #{userId}")
    void unbindAllTaskByProjectId(@Param("userId") @NotNull String userId, @Param("projectId") @NotNull final String projectId);

    @Insert("INSERT INTO `app_task` " +
            "(`id`, `name`, `description`, `status`, `start_date`, `finish_date`, `created_date`, `user_id`, `project_id`) " +
            "VALUES(#{id},#{name},#{description},#{status},#{startDate},#{finishDate},#{created}," +
            "#{userId},#{projectId})")
    void add(@NotNull TaskDTO task);

    @Delete("DELETE FROM `app_task` WHERE `user_id` = #{userId}")
    void clear(final String userId);

    @Delete("DELETE FROM `app_task`")
    void clearAll();

    @Select("SELECT * FROM `app_task` WHERE `user_id` = #{userId}")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAllById(final String userId);

    @Select("SELECT * FROM `app_task`")
    @Result(column = "id", property = "id")
    @Result(column = "start_date", property = "startDate")
    @Result(column = "finish_date", property = "finishDate")
    @Result(column = "description", property = "description")
    @Result(column = "name", property = "name")
    @Result(column = "user_id", property = "userId")
    @Result(column = "status", property = "status")
    @Result(column = "created_date", property = "created")
    @Result(column = "project_id", property = "projectId")
    List<TaskDTO> findAll();

}