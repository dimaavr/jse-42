package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IConnectionSettings {

    @NotNull
    String getJdbcUser();

    @NotNull
    String getJdbcPass();

    @NotNull
    String getJdbcUrl();

    @NotNull
    String getJdbcDriver();

    @NotNull
    String getHibernateDialect();

    @NotNull
    String getHibernateHbm2ddl();

    @NotNull
    String getHibernateShowSql();

}